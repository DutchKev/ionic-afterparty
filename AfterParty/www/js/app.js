var db = null;

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', 'btford.socket-io'])

  .run(function ($ionicPlatform, $rootScope, $state, DB, $window, $cordovaSQLite, $cordovaPush) {

    $ionicPlatform.ready(function () {

      DB.openAndCreateTables();

      var androidConfig = {
        "senderID": "1031110490108"
      };

      $cordovaPush.register(androidConfig).then(function(result) {
        console.log('registerSuccess: ' + result)
      }, function(err) {
        console.log('registerFail: ' + err)
      })

      $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
        switch(notification.event) {
          case 'registered':
            if (notification.regid.length > 0 ) {
              console.log('GCM Registered: ' + notification.regid)
            }
            break;

          case 'message':
            // this is the actual push notification. its format depends on the data model from the push server
            alert('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
            break;

          case 'error':
            alert('GCM error = ' + notification.msg);
            break;

          default:
            alert('An unknown GCM event has occurred');
            break;
        }
      });

      /*
      // WARNING: dangerous to unregister (results in loss of tokenID)
      $cordovaPush.unregister(options).then(function(result) {
        // Success!
      }, function(err) {
        // Error
      })
      */

      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        window.cordova.plugins.Keyboard.disableScroll(true);
      }

      if (window.StatusBar) {
        window.StatusBar.styleLightContent();
      }

    });

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

      if (toState.name !== 'login' && toState.name !== 'register') {

        if (!window.localStorage.getItem('userUID')) {
          event.preventDefault();

          $state.transitionTo('login');
        }
      }
    });
  })

  .config(['$ionicConfigProvider', '$logProvider', function ($ionicConfigProvider, $logProvider) {
    $ionicConfigProvider.backButton.previousTitleText(false).text('');

    $ionicConfigProvider.tabs.position("top");

    //$ionicConfigProvider.navBar.transition('none');

    //$logProvider.debugEnabled(true);

    //$ionicConfigProvider.views.transition('none');
  }])

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    $stateProvider

      .state('register', {
        url: '/register',
        templateUrl: 'templates/register.html',
        controller: 'RegisterCtrl',
        cache: false
      })

      .state('password-reset', {
        url: '/password-reset',
        templateUrl: 'templates/password-reset.html',
        cache: false
      })

      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl',
        cache: false
      })

      .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html',
        cache: true
      })

      .state('tab.chats', {
        url: '/chats',
        views: {
          'tab-chats': {
            templateUrl: 'templates/tab-chats.html',
            controller: 'ChatsCtrl'
          }
        },
        cache: false
      })
      .state('tab.chat-detail', {
        url: '/chats/:userUID',
        views: {
          'tab-chats': {
            templateUrl: 'templates/chat-detail.html',
            controller: 'ChatDetailCtrl'
          }
        },
        cache: true
      })

      .state('tab.users', {
        url: '/users',
        views: {
          'tab-users': {
            templateUrl: 'templates/tab-users.html',
            controller: 'UsersCtrl'
          }
        },
        cache: true
      })

      .state('tab.user-detail', {
        url: '/users/:uid',
        views: {
          'tab-users': {
            templateUrl: 'templates/tab-profile.html',
            controller: 'UserDetailCtrl'
          }
        },
        cache: true
      })

      .state('settings', {
        url: '/settings',
        templateUrl: 'templates/settings.html'
        //controller: 'settingsCtrl',
        //cache: false
      })
    ;

    // if none of the above states are matched, use this as the fallback
    // Bug fix - otherwise $stateChangeStart gets infinite loop (http://stackoverflow.com/a/25755152)
    $urlRouterProvider.otherwise(function ($injector, $location) {
      var $state = $injector.get("$state");
      $state.go("tab.chats");
    });

  });
