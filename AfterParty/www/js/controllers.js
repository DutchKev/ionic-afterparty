angular.module('starter.controllers', [])

  .controller('LoginCtrl', function($scope, $state, $rootScope, LoginService, AUTH_EVENTS) {

    $scope.register = function() {
        $state.go('register');
    };

    $scope.login = function (credentials) {

      if (credentials) {

        LoginService.loginUser(credentials).then(function () {

          //$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);

          $state.go('tab.chats');

        }, function () {

          //$rootScope.$broadcast(AUTH_EVENTS.loginFailed);

        });
      }
    };
  })

  .controller('RegisterCtrl', function($scope, $state, $rootScope, RegisterService, AUTH_EVENTS) {

    $scope.register = function (userData) {
      console.log(userData);
      if (userData) {

        RegisterService.registerUser(userData).then(function (user) {

          console.log(user);

          window.localStorage.setItem('userUID', user.uid);
          window.localStorage.setItem('token', user.token);

        }, function () {
          console.log('fail');
        });

      }
    };
  })

  .controller('ChatsCtrl', ['$scope', '$ionicScrollDelegate', '$timeout', 'Chats', function ($scope, $ionicScrollDelegate, $timeout, Chats) {

    $scope.headerMenu_Show = false;
    $scope.navList = [
      {url: '/settings', title: 'Mijn profiel'},
      {url: '/settings', title: 'Instellingen'},
      {url: '/settings', title: 'Status'}
    ];

    $scope.$on('$ionicView.beforeEnter', function () {

      Chats.getConversations().then(function(conversations) {
        $scope.chats = conversations;
      });

      /*
      $scope.$evalAsync(function () {
        $ionicScrollDelegate.scrollTop();
      });
      */
    });

    $scope.remove = function (chat) {
      Chats.remove(chat);
    };
  }])

  .controller('ChatDetailCtrl', ['$scope', '$stateParams', '$ionicScrollDelegate', 'Chats', 'Users', function ($scope, $stateParams, $ionicScrollDelegate, Chats, Users) {
    var userUID = $stateParams.userUID;

    Chats.getMessages($stateParams.userUID).then(function(messages) {

      messages = messages.map(function(m) {
        m.pos = m.self ? 'right' : 'left';
        return m;
      });

      $scope.messages = messages;
    });

    Users.get($stateParams.userUID).then(function(user) {
      $scope.username = user.username;
    });

    $scope.headerMenu_Show = false;
    $scope.text = '';

    $scope.navList = [
      {url: '/settings', title: 'Mijn profiel'},
      {url: '/settings', title: 'Instellingen'},
      {url: '/settings', title: 'Status'}
    ];

    $scope.$evalAsync(function () {
      $ionicScrollDelegate.scrollBottom();
    });

    $scope.send = function() {
      // Save textarea text
      var message = $scope.text;

      // Cleanup textarea
      $scope.text = '';

      Chats.send(message, userUID).then(function(message) {
        message.pos = 'right';
        $scope.messages.push(message);
      });

    };

  }])

  .controller('UsersCtrl', function ($scope, $window, Users) {
    $scope.headerMenu_Show = false;

    $scope.size = Math.floor($window.innerWidth * 0.23) + 'px';

    $scope.navList = [
      {url: '/settings', title: 'Mijn profiel'},
      {url: '/settings', title: 'Instellingen'},
      {url: '/settings', title: 'Status'}
    ];

    Users.all().then(function(users) {
      $scope.users = users
    });

    //= Users.all();
  })

  .controller('UserDetailCtrl', function ($scope, $stateParams, $ionicScrollDelegate, Users) {
    $scope.liked = false;

    Users.get($stateParams.uid).then(function(user) {

      $scope.user = user;
    });

    $scope.toggleLike = function() {
      $scope.liked = !$scope.liked;
    }
  })

  .controller('AccountCtrl', function ($scope) {
    $scope.settings = {
      enableFriends: true
    };
  });
