angular.module('starter')

  .directive('ngAutoGrow', ['$ionicScrollDelegate', function ($ionicScrollDelegate) {

    function link(scope, element, attrs) {

      var heightLimit = 200;
      var currentHeight = parseInt(window.getComputedStyle(element[0]).height, 10);
      var parentElement = element[0].parentNode.parentNode.parentNode.previousElementSibling;

      element.on('input', function() {

        // Reset hardcoded textarea height
        element[0].style.height = "0px";

        // Calculate new textarea height
        var newHeight = Math.min(element[0].scrollHeight, heightLimit);

        // Set textarea new height
        element[0].style.height = newHeight + "px";

        // Check if messages container should change
        if (newHeight != currentHeight) {

          // Calculate the difference
          var diff = newHeight - currentHeight;

          // Get the messages container current bottom offset
          var currentStyle = parseInt(window.getComputedStyle(parentElement).bottom, 10);

          // Calculate the new bottom offset
          var newMessagesContainerHeight = (currentStyle += diff);

          // Set new bottom offset
          parentElement.style.bottom = newMessagesContainerHeight + 'px';

          // Set to current scrollbar position
          $ionicScrollDelegate.scrollBy(0, diff, true);
        }

        // Store the new textarea height
        currentHeight = newHeight;
      });

      element.on('$destroy', function () {
        element.off('input');
      });
    }

    return {
      link: link
    };
  }]);
