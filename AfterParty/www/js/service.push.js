/**
 * Created by kewinbrandsma on 30/10/15.
 */
angular.module('starter.services')
  .factory('registerPush', function (socketFactory, SERVER) {

    function onSuccess() {
      //console.log(arguments);
    }

    function onError() {
      console.log(arguments);
    }


    if (window.cordova) {
      var pushNotification = window.plugins.pushNotification;
      pushNotification.register(onSuccess, onError,{
        "senderID":"1031110490108",
        "ecb":"app.onNotificationGCM"
      });


      /*

      window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

      var notificationOpenedCallback = function(jsonData) {
        console.log(jsonData);



        console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
      };


      window.plugins.OneSignal.init("98846208-7f26-11e5-ab58-a0369f2d9328",
        {googleProjectNumber: "1031110490108"},
        notificationOpenedCallback);

      // Show an alert box if a notification comes in when the user is in your app.
      window.plugins.OneSignal.enableInAppAlertNotification(true);
      */
    }

  });
