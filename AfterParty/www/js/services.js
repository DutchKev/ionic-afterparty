angular.module('starter.services', [])

  .factory('Token', function () {

    return {
      get: function () {
        return window.localStorage.getItem('token');
      },
      set: function (token) {
        window.localStorage.setItem('token', token);
      }
    }
  })

  .factory('socket', function (socketFactory, SERVER) {

    /*
     var mySocket = socketFactory({

     ioSocket: null //io.connect(SERVER.apiPrefix, {
     //query: "token=" + window.localStorage.getItem('token')
     //})

     });

     mySocket.on('connect', function () {

     mySocket.emit('sync', {}, function () {
     console.log(arguments);
     });

     });
     */

    return {};
    //return mySocket;
  })

  .factory('DB', function ($cordovaSQLite, $q, $ionicPlatform) {
    var db;

    function setup() {

      var clear = false;

      function onSuccess() {
        //console.log(arguments);
      }

      function onError() {
        console.log(arguments);
      }

      if (window.cordova) {

        // App
        if (clear) {
          // Remove old database
          $cordovaSQLite.deleteDB({name: "afterparty.db", location: 1}, onSuccess, onError);
        }

        db = $cordovaSQLite.openDB("afterparty.db");
      } else {

        // Website
        db = window.openDatabase("afterparty.db", "1.0", "AfterParty", -1);

        if (clear) {
          // Clean old database (webSQL has no delete DB function)
          $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS user').then(onSuccess, onError);
          $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS conversation').then(onSuccess, onError);
          $cordovaSQLite.execute(db, 'DROP TABLE IF EXISTS message').then(onSuccess, onError);
        }
      }

      // Build up new Database
      var userTableSQL =
        'CREATE TABLE IF NOT EXISTS user (' +
        'uid int(11) UNIQUE NOT NULL,' +
        'created_at datetime NOT NULL,' +
        'updated_at datetime NOT NULL,' +
        'username text NOT NULL,' +
        'gender int(11) NOT NULL,' +
        'profileIMG text NOT NULL' +
        ');';

      var conversationTableSQL =
        'CREATE TABLE IF NOT EXISTS conversation (' +
        'uid int(11),' +
        'created_at datetime NOT NULL,' +
        'updated_at datetime NOT NULL,' +
        'user_uid int(11) UNIQUE' +
        ');';

      var messageTableSQL =
        'CREATE TABLE IF NOT EXISTS message (' +
        'uid int(11),' +
        'created_at datetime NOT NULL,' +
        'updated_at datetime NOT NULL,' +
        'conversation_uid int(11) NOT NULL,' +
        'message text NOT NULL,' +
        'self BOOLEAN NOT NULL CHECK (self IN (0,1))' +
        ');';

      $cordovaSQLite.execute(db, userTableSQL).then(onSuccess, onError);
      $cordovaSQLite.execute(db, messageTableSQL).then(onSuccess, onError);
      $cordovaSQLite.execute(db, conversationTableSQL).then(onSuccess, onError);
    }

    return {
      openAndCreateTables: setup,
      getDB: function () {
        return db;
      }
    };

  })

  .factory('DBA', function ($cordovaSQLite, $q, $ionicPlatform, DB) {
    var db = DB.getDB();

    var self = this;

    // Handle query's and potential errors
    this.query = function (query, parameters) {
      parameters = parameters || [];
      var q = $q.defer();

      $ionicPlatform.ready(function () {

        $cordovaSQLite.execute(db, query, parameters)
          .then(function (result) {
            q.resolve(result);
          }, function (error) {
            console.warn('I found an error');
            console.warn(error);
            q.reject(error);
          });
      });
      return q.promise;
    };

    // Process a result set
    this.getAll = function (result) {
      var output = [];

      for (var i = 0; i < result.rows.length; i++) {
        output.push(result.rows.item(i));
      }
      return output;
    };

    // Process a single result
    this.getById = function (result) {
      var output = angular.copy(result.rows.item(0));
      return output;
    };

    return self;
  })

  .factory('Request', function ($q, $http, socket, SERVER, Token) {
    var useSocket = false;

    return {
      do: function (opt) {
        var deferred = $q.defer();

        var url = opt.url,
        data = opt.data || {};

        if (useSocket && !opt.forceHTTP) {
          // Websocket
          socket.emit(url, data, function (result) {
            deferred.resolve(result)
          });
        } else {
          // HTTP
          data.token = Token.get();

          $http({
            method: opt.type || 'GET',
            url: SERVER.apiPrefix + url,
            params: data
          }).then(function (result) {
            console.log(result);
            deferred.resolve(result.data)
          }, function () {
            deferred.reject();
          });
        }

        return deferred.promise;
      },


      cancel: function (id) {

      }
    }

  })

  .service('LoginService', function ($q, $http, SERVER) {
    return {
      loginUser: function (credentials) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        promise.success = function (fn) {
          promise.then(fn);
          return promise;
        };

        promise.error = function (fn) {
          promise.then(null, fn);
          return promise;
        };

        $http.post(SERVER.apiPrefix + 'auth', credentials).then(
          function successCallback(response) {

            var data = response.data;

            window.localStorage.setItem('userUID', data.uid);
            window.localStorage.setItem('token', data.token);

            deferred.resolve(data);

            console.log(data);
          },
          function errorCallback(response) {
            deferred.reject(response);
          });

        return promise;
      }
    }
  })

  .service('LogoutService', function ($q, $http, SERVER) {
    return {
      logoutUser: function () {
        var deferred = $q.defer();
        var promise = deferred.promise;

        promise.success = function (fn) {
          promise.then(fn);
          return promise;
        };

        promise.error = function (fn) {
          promise.then(null, fn);
          return promise;
        };

        window.localStorage.removeItem('userUID');
        window.localStorage.removeItem('token');

        window.setTimeout(function () {
          deferred.resolve();
        }, 0);

        return promise;
      }
    }
  })

  .service('RegisterService', function ($q, $http, SERVER) {
    return {
      registerUser: function (userData) {
        var deferred = $q.defer();

        $http.post(SERVER.apiPrefix + 'register', userData).then(
          function successCallback(response) {
            if (response.data.error) {
              deferred.reject(response.data.error);
            } else {
              deferred.resolve(response.data.user);
            }
          },
          function errorCallback(response) {
            deferred.reject(response);
          });

        return deferred.promise;
      }
    }
  })

  .factory('Chats', function ($q, DBA, Request, socket) {
    // Might use a resource here that returns a JSON array

    return {
      getConversations: function () {
        var deferred = $q.defer();

        var query =
          'SELECT c.uid, u.username, m.message, m.created_at, c.user_uid FROM conversation c ' +
          'JOIN user u ON u.uid = c.user_uid ' +
          'JOIN message m ON m.uid = (SELECT uid FROM message mi WHERE mi.conversation_uid = c.uid ORDER BY mi.conversation_uid DESC, mi.uid DESC LIMIT 1) ' +
          'GROUP BY c.uid ' +
          'ORDER BY m.created_at DESC';

        DBA.query(query).then(function (result) {
          var rowsArray = [];

          for (var i = 0, len = result.rows.length; i < len; ++i) {
            rowsArray.push(result.rows.item(i));
          }

          deferred.resolve(rowsArray)
        });

        return deferred.promise;
      },

      getMessages: function (userUID) {
        var deferred = $q.defer();

        if (!userUID) {
          throw new Error('getMessages: userUID is undefined');
        }

        var query =
          'SELECT * FROM message ' +
          'WHERE conversation_uid = (SELECT uid AS c_uid FROM conversation WHERE user_uid=' + userUID + ');';

        DBA.query(query).then(function (result) {
          var rowsArray = [];
          for (var i = 0, len = result.rows.length; i < len; ++i) {
            rowsArray.push(result.rows.item(i));
          }

          console.log(rowsArray);
          deferred.resolve(rowsArray)

          //deferred.resolve(result.rows);
        });

        return deferred.promise;
      },

      insertMessage: function (message) {
        var deferred = $q.defer();

        var query = squel.insert()
          .into("message")
          .set("uid", message.uid)
          .set("created_at", message.created_at)
          .set("updated_at", message.updated_at)
          .set("conversation_uid", message.conversation_uid)
          .set("message", message.message)
          .set("self", 1)
          //.set("level", squel.select().field('MAX(level)').from('levels'))
          .toString();

        DBA.query(query).then(deferred.resolve, deferred.reject);

        return deferred.promise;
      },

      insertConversation: function (conversation) {
        var deferred = $q.defer();

        var selectQuery = squel.select()
          .field("uid")
          .from("conversation")
          .where('user_uid = ' + conversation.userUID)
          .toString();

        // Check if conversation already exists
        DBA.query(selectQuery).then(function (result) {

          if (result.rows.length > 0) {
            // Conversation already exists, so move on
            return deferred.resolve()
          }

          // Insert new conversation
          var query = squel.insert()
            .into("conversation")
            .set("uid", conversation.uid)
            .set("created_at", conversation.created_at)
            .set("updated_at", conversation.updated_at)
            .set("user_uid", conversation.userUID)
            //.set("level", squel.select().field('MAX(level)').from('levels'))
            .toString();

          DBA.query(query).then(deferred.resolve, function (err) {
            // Duplicate entry
            if (err.code === 6) {
              deferred.resolve()
            } else {
              deferred.reject();
            }
          });

        }, function (err) {
        });

        return deferred.promise;
      },

      send: function (message, userUID) {
        var deferred = $q.defer();

        var self = this;

        Request.do({
          url: 'conversation/message',
          data: {
            message: message,
            userUID: userUID
          }
        }).then(function(serverMessage) {

          console.log('servermessage', serverMessage);

          self.insertConversation({
            uid: serverMessage.conversation_uid,
            userUID: userUID
          });

          serverMessage.self = 1; // TODO: Should be done by sever

          self.insertMessage(serverMessage).then(function () {
            deferred.resolve(serverMessage);

          }, deferred.reject);
        }, deferred.reject);

        return deferred.promise;
      },
      remove: function (chat) {
        chats.splice(chats.indexOf(chat), 1);
      }
    };
  })

  .factory('Users', function ($q, Request, DBA) {

    return {
      all: function () {
        var deferred = $q.defer();

        Request.do({url: 'user/list'}).then(function (users) {
          deferred.resolve(users);
        }, function () {
          deferred.reject(false);
        });

        return deferred.promise;
      },

      insert: function (user) {
        var deferred = $q.defer();

        var query =
          'INSERT OR REPLACE INTO user (uid, created_at, updated_at, username, gender, profileIMG) ' +
          'VALUES (?,?, ?, ?, ?, ?)';

        var params = [
          user.uid,
          user.created_at,
          user.updated_at,
          user.username,
          user.gender,
          user.profileIMG
        ];

        DBA.query(query, params).then(deferred.resolve, deferred.reject);

        return deferred.promise;
      },

      get: function (uid) {
        var deferred = $q.defer();

        var self = this;

        var query = 'SELECT * FROM user WHERE uid = ' + uid;

        DBA.query(query).then(function (result) {
          if (result.rows.length) {
            deferred.resolve(result.rows.item(0))
          } else {

            Request.do({
              url: 'user',
              data: {uid: uid}
            }).then(function (user) {

              self.insert(user);

              deferred.resolve(user);
            });
          }

        }, deferred.reject);

        return deferred.promise;
      }
    };
  })
;
